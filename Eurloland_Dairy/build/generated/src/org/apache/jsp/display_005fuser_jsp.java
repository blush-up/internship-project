package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class display_005fuser_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <title>Document</title>\n");
      out.write("\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n");
      out.write("  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n");
      out.write("  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\">\n");
      out.write("\n");
      out.write("\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<style type=\"text/css\">\n");
      out.write("    .brandname{\n");
      out.write("        padding: 5px;\n");
      out.write("        background-color:#ACE36D ;\n");
      out.write("        color: white;\n");
      out.write("        text-align: center;\n");
      out.write("\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    #tagline{\n");
      out.write("        color: #000123;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    </style>\n");
      out.write("\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("    <div class=\"jumbotron brandname\">\n");
      out.write("        <h2>Euroland Dairy</h2>\n");
      out.write("        <p id=\"tagline\">Ireland Dairy Since 1950</p>\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div class=\"container-fluid\">\n");
      out.write("        \n");
      out.write("        ");

            String name = (String)session.getAttribute("user");
            
            out.print("Welcome "+name);
            
        
      out.write("\n");
      out.write("        <br> <br>\n");
      out.write("        <table class=\"table table-bordered\" >\n");
      out.write("            <tr bgcolor=\"gold\">\n");
      out.write("                <th>Sr.NO</th>\n");
      out.write("                <th>First Name</th>\n");
      out.write("                <th>Last Name</th>\n");
      out.write("                <th>Phone No</th>\n");
      out.write("                <th>Email Id</th>\n");
      out.write("                <th>Username</th>\n");
      out.write("                <th>Password</th>\n");
      out.write("                <th>Action</th>\n");
      out.write("\n");
      out.write("            </tr>\n");

try{ 
           Class.forName("com.mysql.jdbc.Driver");
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dairy_db", "root", "");

	Statement st=conn.createStatement();
	String sql ="select * from new_user ";
	ResultSet  rs = st.executeQuery(sql);

	while(rs.next())
	{
	
      out.write("\n");
      out.write("\n");
      out.write("\t<tr>\n");
      out.write("            <td>");
      out.print(rs.getString("user_id") );
      out.write("</td>\n");
      out.write("\t  <td>");
      out.print(rs.getString("fname") );
      out.write("</td>\n");
      out.write("\t  <td>");
      out.print(rs.getString("lname") );
      out.write("</td>\n");
      out.write("\t  <td>");
      out.print(rs.getString("phone_no") );
      out.write("</td>\n");
      out.write("\t  <td>");
      out.print(rs.getString("email_id") );
      out.write("</td>\n");
      out.write("\t  <td>");
      out.print(rs.getString("uname") );
      out.write("</td>\n");
      out.write("\t  <td>");
      out.print(rs.getString("pass") );
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td align='center'> \n");
      out.write("                   <button class=\"btn btn-info\">\n");
      out.write("                       <a href=\"update_user.jsp?id=");
      out.print(rs.getString("user_id"));
      out.write("\"> <span class=\"glyphicon glyphicon-pencil\"></span>    Update </a>\n");
      out.write("                  </button> \n");
      out.write("              \n");
      out.write("                     <button class=\"btn btn-info\">    \n");
      out.write("                         <a href=\"deleteuser.jsp?id=");
      out.print(rs.getString("user_id"));
      out.write("\"> <span class=\"glyphicon glyphicon-trash\"></span>   Delete</a>  \n");
      out.write("                   </button>\n");
      out.write("            </td>\n");
      out.write("\n");
      out.write("                </tr>\n");
      out.write("\t");

	}
              
              conn.close();
} 
catch (Exception e)
 {
   System.out.print(e);
}

      out.write("\n");
      out.write("</table> \n");
      out.write("</center>\n");
      out.write("                       \n");
      out.write("<center>\n");
      out.write("           <a href=\"dashboard_admin.jsp\">\n");
      out.write("                      Go to Dashboard\n");
      out.write("             </a>\n");
      out.write("</center>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
