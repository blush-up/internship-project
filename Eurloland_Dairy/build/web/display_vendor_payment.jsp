<%@ page import="java.sql.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">


</script>

<style type="text/css">
    .brandname{
        padding: 5px;
        background-color:#ACE36D ;
        color: white;
        text-align: center;

    }

    #tagline{
        color: #000123;
    }

    </style>


</head>
<body>

    <div class="jumbotron brandname">
        <h2>Euroland Dairy</h2>
        <p id="tagline">Ireland Dairy Since 1950</p>

    </div>

    <div class="container-fluid">
        <table class="table table-bordered" >
            <tr bgcolor="gold">
                <th>Sr.No</th>
                 <th>Payment Date</th>
                 <th>Payment Time</th>     
                <th>First Name</th>
                <th>Last Name</th>
                <th>Payment Amount</th>
                <th>Business Nature</th>
                <th>Payment Mode</th>  
                <th>Remark</th>
                <th>Paid By</th>
                <th>Action</th>


            </tr>
<%
try{ 
           Class.forName("com.mysql.jdbc.Driver");
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dairy_db", "root", "");

	Statement st=conn.createStatement();
	String sql ="select * from vendor_payment ";
	ResultSet  rs = st.executeQuery(sql);

	while(rs.next())
	{
	%>

	<tr>
             <td><%=rs.getString("vendor_id") %></td>
            <td><%=rs.getString("p_date") %></td>
	  <td><%=rs.getString("p_time") %></td>
	  <td><%=rs.getString("fname") %></td>
	  <td><%=rs.getString("lname") %></td>
	  <td><%=rs.getString("p_amt") %></td>
	  <td><%=rs.getString("business_nature") %></td>
	  <td><%=rs.getString("mode") %></td>
          <td><%=rs.getString("remark") %></td>
      	  <td><%=rs.getString("paid_by") %></td>



                <td align='center'> 
                   <button class="btn btn-info">
                      <span class="glyphicon glyphicon-pencil"></span>    Update 
                  </button> 
              
                     <button class="btn btn-info">    
                       <span class="glyphicon glyphicon-trash"></span>   Delete  
                   </button>
            </td>

                </tr>
	<%
	}
              
              conn.close();
} 
catch (Exception e)
 {
   System.out.print(e);
}
%>
</table> 
</center>
                       
<center>
           <a href="index.html">
                      Go to User Registration Form
             </a>
</center>

</body>
</html>
