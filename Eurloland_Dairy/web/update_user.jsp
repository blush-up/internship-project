<%@page import="java.sql.*"%> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>www.euralanddairy.com</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
</script>

<style type="text/css">
    .brandname{
        padding: 5px;
        background-color:#ACE36D ;
        color: white;
        text-align: center;

    }

    #tagline{
        color: #000123;
    }

   .well{
    background-color: gold;
    font-size: 20px;
   }

   .well:hover{
    background-color: limegreen;
    color: white;
    cursor: pointer;
   }

   .cardcontainer{
    box-shadow: 0px 5px 10px rgba(0, 0, 0,0.5);
    transition: 0.3s;
    border: 3px solid#E34CE1;

   }

   .cardcontainer:hover{
    box-shadow: 0px 10px 20px rgba(0, 0, 0,0.5);
   }

   .carddata{
    padding: 20px;
    padding-bottom: 10px;
    width: 100%;
    background-color: #E34CE1;
    color: white;
   }

   .amountclass{
    font-size: 20px;
   }

   .enq_form{
    background-color: gold;
    padding: 20px;
    width: 40%;
    margin-left: 30%;
   }
</style>




</head>
<body>

    <div class="jumbotron brandname">
        <h2>Euroland Dairy</h2>
        <p id="tagline">Ireland Dairy Since 1950</p>

    </div>
    
    
    <%
        String id = request.getParameter("id");
        
        try{ 
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dairy_db", "root", "");
            Statement st=conn.createStatement();
            
            String sql = "select * from new_user where user_id="+id;
            
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next())
            {
             %>   
            
   

    <div class="container-fluid ">

        <div class="row enq_form ">

            <h5 class="text-center">Update User</h5>

            <form class="form" name="f1" action="update_user_process.jsp">
            <input type="hidden" name="user_id" value="<%=rs.getString("user_id")%>">
                <div class="form-group">
                    <label for="">First Name</label>
                    <input type="text" name="fname" value="<%=rs.getString("fname")%>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Last Name</label>
                    <input type="text" name="lname" value="<%=rs.getString("lname")%>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Phone No</label>
                    <input type="number" name="phone_no" value="<%=rs.getString("phone_no")%>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Email Id</label>
                    <input type="text" name="email_id" value="<%=rs.getString("email_id")%>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">User Name</label>
                    <input type="text" name="uname" value="<%=rs.getString("uname")%>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Password</label>
                    <input type="text" name="pass" value="<%=rs.getString("pass")%>" class="form-control">
                </div>



                


                    <div class="form-group text-center">
                        <button class="btn btn-primary" type="submit"> Update User</button>
                        <button class="btn btn-primary">Reset User</button>

                    </div>

                
            </form>

        </div>  <!--end of enq_form-->  

   
    </div> 
     <%
	}
              
              conn.close();
} 
catch (Exception e)
 {
   System.out.print(e);
}
%> 
            
</body>
</html>