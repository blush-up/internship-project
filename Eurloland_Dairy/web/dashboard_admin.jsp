<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
</script>

<style>
    .brandname
    {
        padding: 10px;
        background-color: #4CE36D ;
        color: white;
        text-align: center;
    }
    #tagline
    {
        color: #000123;
    }
    .well
    {
        background-color: gold;
    }
    .well:hover
    {
        background-color: #4CE36D;
        color: white;
        cursor: pointer;
    }
    a{
        text-decoration: none;
        color: black;
        
    }
   a:hover
   {
    color: white;
    text-decoration: none;
   }
   body{
    background-image: url("pics/00.jpeg");
    background-image: cover;
   }
   h3{
        color:white;
        padding: 20px;
    }
    .session_name{
        color:white;
        font-size: 20px;
        margin-left: 45%;
        
        
    }

</style>

</head>
<body>

    <div class="jumbotron brandname">

        <h2>Euroland Dairy</h2>
        <p id="tagline">Ireland Dairy Since 1950</p>



    </div>

    <div class="container-fluid  ">
        <%
          
           
            String sname = (String)session.getAttribute("user");
            
            out.print("<span class = 'session_name'>Welcome "+sname+"</span>");
            
        
        %>

        <br><br><br>

       <div class="row text-center">

            
        
            <div class="col-sm-3">
                <div class="well"><a href="create_user.html">Create User</a></div>
            </div>
            
            <div class="col-sm-3">
                <div class="well"><a href="display_user.jsp">Display User</a></div>
            </div>

            <div class="col-sm-3">
                <div class="well"><a href="display_update.jsp">Update User</a> </div>
            </div>

            <div class="col-sm-3">
                <div class="well"><a href="display_remove.jsp">Remove User</a></div>
            </div>
            
       </div>

    <!---End of Container-Fluid div-->

    

        <div class="row text-center">       
 
             <div class="col-sm-3">
                 <div class="well"><a href="client_payment.html">Client Payment</a></div>
             </div>
             
             <div class="col-sm-3">
                 <div class="well"><a href="vendor_payment.html">Vendor Payment</a></div>
             </div>
 
             <div class="col-sm-3">
                 <div class="well"><a href="employee_payment.html">Employee Payment</a></div>
             </div>
 
             <div class="col-sm-3">
                 <div class="well"><a href="leadger.book.html">Ledger Book</a></div>
             </div>
             
        </div>
        
    
    
    
        
 
       
        <div class="row text-center">       
 
             <div class="col-sm-3">
                 <div class="well"><a href="display_client_payment.jsp">Display Client Payment</a></div>
             </div>
             
             <div class="col-sm-3">
                 <div class="well"><a href="display_client_payment.jsp">Display Vendor Payment</a></div>
             </div>
 
             <div class="col-sm-3">
                 <div class="well"><a href="display_employee_payment.jsp">Display Employee Payment</a></div>
             </div>
 
             <div class="col-sm-3">
                 <div class="well"><a href="#">Add Task</a></div>
             </div>
             
        </div>

        
 
     </div> <!---End of Container-Fluid div-->
</body>

</html>